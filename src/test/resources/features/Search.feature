Feature: Feature for inpost

  Scenario Outline: Paczki o okreslonych numerach znajdują się w odpowiednich statusach
    Given Użytkownik znajduje się na stronie głównej i zaakceptował cookies
    When Użytkownik wypełnia input numeru przesyłki danymi "<numer przesylki>"
    And Użytkownik klika na guzik znajdź
    Then Przesyłka znaleziona i status to "<status>"

    Examples:
      | numer przesylki          | status       |
      | 675220159157550132544277 | Dostarczona. |
      | 602677159157340025039205 | Dostarczona. |
#    |502677159157340025039205         |                   | <- ten numer niestety nie działa
