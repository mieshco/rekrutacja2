package stepdefinitions;


import cucumber.api.java.en.Then;
import org.junit.Assert;
import pom.InpostSearchPage;

public class InpostSearchSteps extends InpostSearchPage {

    @Then("Przesyłka znaleziona i status to {string}")
    public void statusOfPackageEquals(String value) {
        Assert.assertEquals(String.format("We expected &s status, but we got &s instead", value, activeStatusElement.getText())
                ,value, activeStatusElement.getText());
    }

}
