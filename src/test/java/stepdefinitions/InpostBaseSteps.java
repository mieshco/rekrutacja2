package stepdefinitions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import org.junit.Assert;
import pom.InpostBasePage;

public class InpostBaseSteps extends InpostBasePage {

    @Given("Użytkownik znajduje się na stronie głównej i zaakceptował cookies")
    public void userOnMainPage() {
        Assert.assertTrue(followPackageHeader.isDisplayed());
        acceptCookiesButton.click();
    }

    @When("Użytkownik wypełnia input numeru przesyłki danymi {string}")
    public void userFillInputWithPackageNumber(String value) {
        packageNumberInput.sendKeys(value);
    }

    @And("Użytkownik klika na guzik znajdź")
    public void userClickOnFindButton() {
        findPackageButton.click();
    }
}
