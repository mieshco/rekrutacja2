package pom;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.Hooks;


public class InpostSearchPage {

    @FindBy(css = "div[class='single--status--block -active'] p[class='paragraph--component -big -secondary']")
    public WebElement activeStatusElement;

    public InpostSearchPage() {
        PageFactory.initElements(Hooks.driver, this);
    }


}
