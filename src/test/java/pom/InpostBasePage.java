package pom;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.Hooks;


public class InpostBasePage {

    @FindBy(css = "h2[class='header--h2 -white -secondary']")
    public WebElement followPackageHeader;

    @FindBy(css = "button[id='onetrust-accept-btn-handler']")
    public WebElement acceptCookiesButton;

    @FindBy(css = "input[class='form--control inputForMat']")
    public WebElement packageNumberInput;

    @FindBy(css = "button[class='btn--primary -mobilefull']")
    public WebElement findPackageButton;

    public InpostBasePage() {
        PageFactory.initElements(Hooks.driver, this);
    }


}
